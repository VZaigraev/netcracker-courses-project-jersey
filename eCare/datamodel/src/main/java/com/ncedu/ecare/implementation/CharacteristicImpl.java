package com.ncedu.ecare.implementation;

import com.ncedu.ecare.datamodel.CharType;
import com.ncedu.ecare.datamodel.Characteristic;

import java.math.BigDecimal;
import java.util.HashMap;


public class CharacteristicImpl<T> extends AbstractEntityImpl implements Characteristic<T>{
    private T value;
    private CharType charType;

    public CharacteristicImpl() {
        value = null;
        charType = CharType.NONE;
    }

    public T getValue(){
        return value;
    }

    public void setValue(T value) throws ClassCastException{
        this.value = value;
    }
    public CharType getCharType() {
        return charType;
    }

    public void setCharType(CharType charType) {
        this.charType = charType;
    }

    public HashMap<String,Object> toHashMap()   {
        HashMap<String,Object> rawValue = new HashMap<String, Object>();
        rawValue.put("ATTRIBUTE_NAME",getName());
        rawValue.put("ATTRIBUTE_DESC",getDescription());
        rawValue.put("ATTRIBUTE_ID",BigDecimal.valueOf(getId()));
        switch (charType){
            case STRING:
                rawValue.put("TEXT_VALUE",value);
                break;
            case DECIMAL:
                rawValue.put("NUMBER_VALUE", BigDecimal.valueOf((Long)value));
                break;
            case DATE:
                rawValue.put("DATE_VALUE", value);
        }
        return rawValue;
    }

    @Override
    public String toString() {
        return "CharacteristicObject{"+super.toString() +
                "value=" + value +
                ", charType=" + charType +
                '}';
    }
}

package com.ncedu.ecare.datamodel;

public interface Phone extends Product {
    public String getOperatingSystem();
    public void setOperatingSystem(String operatingSystem);
    public Long getScreen();
    public void setScreen(Long screen);
    public String getColor();
    public void setColor(String color);


}

package com.ncedu.ecare.datamodel;

public enum CharType {
    STRING("TEXT_VALUE"),
    DATE("DATE_VALUE"),
    DECIMAL("NUMBER_VALUE"),
    NONE(null);
    private String sql;

    CharType(String sql) {
        this.sql = sql;
    }

    public String getSql() {
        return sql;
    }
}

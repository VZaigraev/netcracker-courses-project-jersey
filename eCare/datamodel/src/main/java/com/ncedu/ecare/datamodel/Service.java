package com.ncedu.ecare.datamodel;

public interface Service extends Product {
    Long getSms();
    void setSms(Long sms);
    Long getCalls();
    void setCalls(Long calls);
    Long getInternet();
    void setInternet(Long internet);
}

package com.ncedu.ecare.datamodel;

import java.util.List;

public interface SalesOrder extends Data {
    void setDate(String date);
    String getDate();
    void setClient ( Long client);
    Long getClient();
    void setProducts(List<Long> products);
    List<Long> getProducts();
}

package com.ncedu.ecare.datamodel;

public enum ObjectTypeId {
    SERVICE(0L),PHONE(1L),ACCESSORY(2L),CLIENT(3L),SALESORDER(4L),DATA(-1L);
    private Long value;

    ObjectTypeId(Long value) {
        this.value = value;
    }

    public static Long getValue(String name){
        try {
            return valueOf(name.toUpperCase()).value;
        }
        catch (Exception e){
            return DATA.value;
        }
    }

    public Long getValue() {
        return value;
    }
}

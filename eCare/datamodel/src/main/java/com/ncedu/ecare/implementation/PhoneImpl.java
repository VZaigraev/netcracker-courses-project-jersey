package com.ncedu.ecare.implementation;

import com.ncedu.ecare.datamodel.CharType;
import com.ncedu.ecare.datamodel.Characteristic;
import com.ncedu.ecare.datamodel.CharacteristicId;
import com.ncedu.ecare.datamodel.Phone;

import java.util.List;


public class PhoneImpl extends ProductImpl implements Phone{
    public PhoneImpl() {
        operatingSystem=null;
        screen = null;
        color = null;
    }

    private Characteristic<String> operatingSystem;
    private Characteristic<Long> screen;
    private Characteristic<String> color;

    @Override
    public List<Characteristic> takeChars() {
        List<Characteristic> chars = super.takeChars();
        chars.add(operatingSystem);
        chars.add(screen);
        chars.add(color);
        return chars;
    }

    public String getOperatingSystem() {
        return operatingSystem.getValue();
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = new CharacteristicImpl<String>();
        this.operatingSystem.setCharType(CharType.STRING);
        this.operatingSystem.setId(CharacteristicId.OPERATINGSYSTEM.getValue());
        this.operatingSystem.setValue(operatingSystem);
    }

    public void putChars(List<Characteristic> chars) {
        for (Characteristic ch : chars) {
            Long id = ch.getId();
            if(CharacteristicId.OPERATINGSYSTEM.getValue().equals(id))
                operatingSystem = ch;
            else if (CharacteristicId.COLOR.getValue().equals(id))
                color = ch;
            else if(CharacteristicId.SCREENSIZE.getValue().equals(id))
                screen = ch;
            else if(CharacteristicId.PRICE.getValue().equals(id))
                putPrice(ch);
        }
    }

    public Long getScreen() {
        return screen.getValue();
    }

    public void setScreen(Long screen) {
        this.screen = new CharacteristicImpl<Long>();
        this.screen.setCharType(CharType.DECIMAL);
        this.screen.setId(CharacteristicId.SCREENSIZE.getValue());
        this.screen.setValue(screen);
    }

    public String getColor() {
        return color.getValue();
    }

    public void setColor(String color) {
        this.color = new CharacteristicImpl<String>();
        this.color.setCharType(CharType.DECIMAL);
        this.color.setId(CharacteristicId.COLOR.getValue());
    }

    @Override
    public String toString() {
        return "PhoneImpl{" +
                "operatingSystem=" + operatingSystem +
                ", screen=" + screen +
                ", color=" + color +
                '}';
    }
}

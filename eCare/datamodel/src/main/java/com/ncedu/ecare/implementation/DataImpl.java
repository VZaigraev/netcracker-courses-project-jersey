package com.ncedu.ecare.implementation;

import com.ncedu.ecare.datamodel.Characteristic;
import com.ncedu.ecare.datamodel.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public abstract class DataImpl extends AbstractEntityImpl implements Data{

    public DataImpl() {
    }


    public  List<Characteristic> takeChars(){
         return new ArrayList<Characteristic>();
    }
    @Override
    public String toString() {
        return "DataObject{" + super.toString() +
                '}';
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String,Object> rawValue = new HashMap<String, Object>();
        rawValue.put("OBJECT_NAME",getName());
        rawValue.put("OBJECT_DESC",getDescription());
        rawValue.put("OBJECT_ID", BigDecimal.valueOf(getId()));
        return rawValue;
    }
}

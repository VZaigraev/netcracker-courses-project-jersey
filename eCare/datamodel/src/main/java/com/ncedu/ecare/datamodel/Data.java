package com.ncedu.ecare.datamodel;

import java.util.List;

public interface Data extends AbstractEntity{
    List<Characteristic> takeChars();
    void putChars(List<Characteristic> chars);
}

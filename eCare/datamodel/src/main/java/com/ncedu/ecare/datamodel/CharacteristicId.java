package com.ncedu.ecare.datamodel;

public enum CharacteristicId {
    INTERNET(0L,CharType.DECIMAL),SMS(1L,CharType.DECIMAL), CALLS(2L,CharType.DECIMAL),
    SCREENSIZE(3L,CharType.DECIMAL), OPERATINGSYSTEM(4L,CharType.STRING), PRICE(5L,CharType.DECIMAL),
    COLOR(6L,CharType.STRING), PHONES(7L,CharType.DECIMAL), ACCESSORYTYPE(8L,CharType.STRING),
    ADDRESS(9L,CharType.STRING), FIO(10L,CharType.STRING), DATE(11L,CharType.DATE), CLIENT(13L,CharType.DECIMAL),
    PRODUCTS(12L,CharType.DECIMAL), NAME(-1L,CharType.STRING);
    private Long value;
    private CharType type;
    CharacteristicId(Long value,CharType type) {
        this.value = value;
        this.type =type;
    }

    public static Long getValue(String name){
        try {
            return valueOf(name.toUpperCase()).value;
        }
        catch (Exception e){
            return NAME.value;
        }
    }

    public Long getValue() {
        return value;
    }
    public static String getType(String name) {
        try {
            return valueOf(name.toUpperCase()).type.getSql();
        }
        catch (Exception e){
            return NAME.type.getSql();
        }
    }

}

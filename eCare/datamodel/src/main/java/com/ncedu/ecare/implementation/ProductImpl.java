package com.ncedu.ecare.implementation;

import com.ncedu.ecare.datamodel.CharType;
import com.ncedu.ecare.datamodel.Characteristic;
import com.ncedu.ecare.datamodel.CharacteristicId;
import com.ncedu.ecare.datamodel.Product;

import java.util.List;

public abstract class ProductImpl extends DataImpl implements Product {
    private Characteristic<Long> price;

    public ProductImpl() {
        price = null;
    }
    public List<Characteristic> takeChars(){
        List<Characteristic> chars = super.takeChars();
        chars.add(price);
        return chars;
    }
    protected void putPrice(Characteristic price) {
        this.price= price;
    }


    public Long getPrice() {
        return price.getValue();
    }
    public void setPrice(Long price) {
        this.price = new CharacteristicImpl<Long>();
        this.price.setCharType(CharType.DECIMAL);
        this.price.setValue(price);
        this.price.setId(CharacteristicId.PRICE.getValue());
    }
}

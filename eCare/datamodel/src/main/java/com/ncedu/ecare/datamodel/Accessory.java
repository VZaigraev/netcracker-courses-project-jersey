package com.ncedu.ecare.datamodel;

import java.util.List;

public interface Accessory extends Product {
    void setType(String type);
    String getType();
    void setColor (String color);
    String getColor();
    void setPhones(List<Long> phones);
    List<Long> getPhones();
}

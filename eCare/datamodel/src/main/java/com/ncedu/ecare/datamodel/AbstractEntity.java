package com.ncedu.ecare.datamodel;

import java.io.Serializable;
import java.util.HashMap;

public interface AbstractEntity extends Serializable{
    Long getId();
    void setId(Long id);

    String getName();
    void setName(String name);

    String getDescription();
    void setDescription(String description);

    HashMap<String,Object> toHashMap();
}

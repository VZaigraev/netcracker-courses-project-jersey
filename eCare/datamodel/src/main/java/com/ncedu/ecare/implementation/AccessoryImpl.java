package com.ncedu.ecare.implementation;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ncedu.ecare.datamodel.Accessory;
import com.ncedu.ecare.datamodel.CharType;
import com.ncedu.ecare.datamodel.Characteristic;
import com.ncedu.ecare.datamodel.CharacteristicId;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(value = {"compatible"})
public class AccessoryImpl extends ProductImpl implements Accessory {
    public AccessoryImpl() {
    }

    private Characteristic<String> type;
    private Characteristic<String> color;
    private List<Characteristic<Long>> compatible;


    @Override
    public List<Characteristic> takeChars() {
        List<Characteristic> chars = super.takeChars();
        chars.add(type);
        chars.addAll(compatible);
        chars.add(color);
        return chars;
    }

    public void putChars(List<Characteristic> chars) {
        for (Characteristic ch : chars) {
            Long id = ch.getId();
            if(CharacteristicId.ACCESSORYTYPE.getValue().equals(id))
                type = ch;
            else if (CharacteristicId.COLOR.getValue().equals(id))
                color = ch;
            else if(CharacteristicId.PHONES.getValue().equals(id)) {
                if (compatible == null) compatible = new ArrayList<Characteristic<Long>>();
                compatible.add(ch);
            }
            else if(CharacteristicId.PRICE.getValue().equals(id))
                putPrice(ch);
        }
    }

    public void setType(String type) {
        this.type = new CharacteristicImpl<String>();
        this.type.setCharType(CharType.STRING);
        this.type.setId(CharacteristicId.ACCESSORYTYPE.getValue());
        this.type.setValue(type);
    }

    public String getType() {
        return type.getValue();
    }

    public void setColor(String color) {
        this.color = new CharacteristicImpl<String>();
        this.color.setCharType(CharType.STRING);
        this.color.setId(CharacteristicId.COLOR.getValue());
        this.color.setValue(color);
    }

    public String getColor() {
        return color.getValue();
    }



    public void setPhones(List<Long> phones) {
        this.compatible = new ArrayList<Characteristic<Long>>();
        for (Long value: phones) {
            CharacteristicImpl<Long> chr = new CharacteristicImpl<Long>();
            chr.setCharType(CharType.DECIMAL);
            chr.setId(CharacteristicId.PHONES.getValue());
            chr.setValue(value);
            this.compatible.add(chr);
        }
    }



    public List<Long> getPhones() {
        List<Long> value = new ArrayList<Long>();
        for(Characteristic<Long> characteristic: compatible){
            value.add(characteristic.getValue());
        }
        return value;
    }

    @Override
    public String toString() {
        return "AccessoryImpl{" +
                "type=" + type +
                ", color=" + color +
                ", compatible=" + compatible +
                '}';
    }
}

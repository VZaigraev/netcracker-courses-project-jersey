package com.ncedu.ecare.datamodel;

public interface Characteristic<T> extends AbstractEntity {
    T getValue();
    void setValue(T value);
    CharType getCharType();
    void setCharType(CharType charType);
}

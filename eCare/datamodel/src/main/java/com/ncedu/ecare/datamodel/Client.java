package com.ncedu.ecare.datamodel;

public interface Client extends Data {
    void setFio(String fio);
    String getFio();
    void setAddress(String address);
    String getAddress();
}

package com.ncedu.ecare.implementation;

import com.ncedu.ecare.datamodel.CharType;
import com.ncedu.ecare.datamodel.Characteristic;
import com.ncedu.ecare.datamodel.CharacteristicId;
import com.ncedu.ecare.datamodel.Client;

import java.util.List;

public class ClientImpl extends DataImpl implements Client {
    public ClientImpl() {
        fio = null;
        address = null;
    }

    private Characteristic<String> fio;
    private Characteristic<String> address;



    public void putChars(List<Characteristic> chars) {
        for (Characteristic ch: chars) {
            Long id = ch.getId();
            if(CharacteristicId.FIO.getValue().equals(id))
                fio = ch;
            else if(CharacteristicId.ADDRESS.getValue().equals(id))
                address = ch;
        }
    }

    @Override
    public List<Characteristic> takeChars() {
        List<Characteristic> chars =  super.takeChars();
        chars.add(fio);
        chars.add(address);
        return chars;
    }

    public void setFio(String fio) {
        this.fio = new CharacteristicImpl<String>();
        this.fio.setCharType(CharType.STRING);
        this.fio.setId(CharacteristicId.FIO.getValue());
        this.fio.setValue(fio);
    }

    public String getFio() {
        return fio.getValue();
    }

    public void setAddress(String address) {
        this.address = new CharacteristicImpl<String>();
        this.address.setCharType(CharType.STRING);
        this.address.setId(CharacteristicId.ADDRESS.getValue());
        this.address.setValue(address);
    }

    public String getAddress() {
        return address.getValue();
    }

    @Override
    public String toString() {
        return "ClientImpl{" +
                "fio=" + fio +
                ", address=" + address +
                '}';
    }
}

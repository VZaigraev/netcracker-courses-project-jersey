package com.ncedu.ecare.implementation;

import com.ncedu.ecare.datamodel.CharType;
import com.ncedu.ecare.datamodel.Characteristic;
import com.ncedu.ecare.datamodel.CharacteristicId;
import com.ncedu.ecare.datamodel.Service;

import java.util.List;


public class ServiceImpl extends ProductImpl implements Service {
    public ServiceImpl() {
        sms = null;
        calls = null;
        internet = null;
    }

    private Characteristic<Long> sms;
    private Characteristic<Long> calls;
    private Characteristic<Long> internet;

    @Override
    public List<Characteristic> takeChars() {
        List<Characteristic> chars = super.takeChars();
        chars.add(sms);
        chars.add(calls);
        chars.add(internet);
        return chars;
    }

    public void putChars(List<Characteristic> chars) {
        for (Characteristic ch: chars) {
            Long id = ch.getId();
            if (CharacteristicId.SMS.getValue().equals(id)){
                sms = ch;
            } else if (CharacteristicId.CALLS.getValue().equals(id)){
                calls = ch;
            } else if (CharacteristicId.INTERNET.getValue().equals(id)){
                internet = ch;
            } else if(CharacteristicId.PRICE.getValue().equals(id)){
                putPrice(ch);
            }
        }
    }

    public Long getSms() {
        return sms.getValue();
    }


    public void setSms(Long sms) {
        this.sms = new CharacteristicImpl<Long>();
        this.sms.setCharType(CharType.DECIMAL);
        this.sms.setValue(sms);
        this.sms.setId(CharacteristicId.SMS.getValue());
    }

    public Long getCalls() {
        return calls.getValue();
    }


    public void setCalls(Long calls) {
        this.calls = new CharacteristicImpl<Long>();
        this.calls.setCharType(CharType.DECIMAL);
        this.calls.setValue(calls);
        this.calls.setId(CharacteristicId.CALLS.getValue());
    }


    public Long getInternet() {
        return internet.getValue();
    }


    public void setInternet(Long internet) {
        this.internet = new CharacteristicImpl<Long>();
        this.internet.setCharType(CharType.DECIMAL);
        this.internet.setValue(internet);
        this.internet.setId(CharacteristicId.INTERNET.getValue());
    }

    @Override
    public String toString() {
        return "ServiceImpl{" +
                "sms=" + sms +
                ", calls=" + calls +
                ", internet=" + internet +
                '}';
    }
}

package com.ncedu.ecare.implementation;

import com.ncedu.ecare.datamodel.CharType;
import com.ncedu.ecare.datamodel.Characteristic;
import com.ncedu.ecare.datamodel.CharacteristicId;
import com.ncedu.ecare.datamodel.SalesOrder;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SalesOrderImpl extends DataImpl implements SalesOrder{

    private Characteristic<Timestamp> date;
    private Characteristic<Long> client;
    private List<Characteristic<Long>> products;

    public void putChars(List<Characteristic> chars) {
        for (Characteristic ch : chars) {
            Long id = ch.getId();
            if(CharacteristicId.CLIENT.getValue().equals(id))
                client = ch;
            else if (CharacteristicId.DATE.getValue().equals(id))
                date = ch;
            else if(CharacteristicId.PRODUCTS.getValue().equals(id)) {
                if (products == null) products = new ArrayList<Characteristic<Long>>();
                products.add(ch);
            }
        }
    }

    @Override
    public List<Characteristic> takeChars() {
        List<Characteristic> chars =  super.takeChars();
        chars.addAll(products);
        chars.add(client);
        chars.add(date);
        return chars;
    }

    public void setDate(String date) {
        this.date = new CharacteristicImpl<Timestamp>();
        this.date.setValue(Timestamp.valueOf(date));
        this.date.setCharType(CharType.DATE);
        this.date.setId(CharacteristicId.DATE.getValue());
    }

    public String getDate() {
        return date.getValue().toString();
    }

    public void setClient(Long client) {
        this.client = new CharacteristicImpl<Long>();
        this.client.setCharType(CharType.DECIMAL);
        this.client.setId(CharacteristicId.CLIENT.getValue());
        this.client.setValue(client);
    }

    public Long getClient() {
        return client.getValue();
    }

    public void setProducts(List<Long> products) {
        this.products = new ArrayList<Characteristic<Long>>();
        for (Long value: products) {
            Characteristic<Long> characteristic = new CharacteristicImpl<Long>();
            characteristic.setCharType(CharType.DECIMAL);
            characteristic.setId(CharacteristicId.PRODUCTS.getValue());
            characteristic.setValue(value);
            this.products.add(characteristic);
        }
    }

    public List<Long> getProducts() {
        List<Long> list = new ArrayList<Long>();
        for (Characteristic<Long> value: products) {
            list.add(value.getValue());
        }
        return list;
    }

    @Override
    public String toString() {
        return "SalesOrderImpl{" +
                "date=" + date +
                ", client=" + client +
                ", products=" + products +
                '}';
    }
}

package com.ncedu.ecare.datamodel;

public interface Product extends Data{
    void setPrice(Long price);
    Long getPrice();
}

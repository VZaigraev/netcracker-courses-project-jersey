package com.ncedu.ecare.implementation;

import com.ncedu.ecare.datamodel.CharType;
import com.ncedu.ecare.datamodel.Characteristic;
import com.ncedu.ecare.datamodel.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;


public abstract class DataObjectFactory {
    public static Data getObject(Long id, HashMap entityMap, List<Characteristic> chars) {
        BigDecimal rawId = (BigDecimal) entityMap.get("OBJECT_TYPE_ID");
        Data data = null;
        switch (rawId.intValue()){
            case 0:
                 data = new ServiceImpl();
                 break;
            case 1:
                data = new PhoneImpl();
                break;
            case 2:
                data = new AccessoryImpl();
                break;
            case 3:
                data  = new ClientImpl();
                break;
            case 4:
                data = new SalesOrderImpl();
                break;
            default:
                return null;
        }
        data.setId(id);
        data.setName((String)entityMap.get("OBJECT_NAME"));
        data.setDescription((String)entityMap.get("OBJECT_DESCRIPTION"));
        data.putChars(chars);
        return data;
    }

    public static Characteristic getChar(HashMap<String,Object> map) {
        Characteristic ch = null;
        if(map.get("TEXT_VALUE")!= null) {
            ch = new CharacteristicImpl<String>();
            ch.setCharType(CharType.STRING);
            ch.setValue(map.get("TEXT_VALUE"));
        }
        else if (map.get("NUMBER_VALUE")!= null) {
            ch = new CharacteristicImpl<Long>();
            ch.setCharType(CharType.DECIMAL);
            Object rawValue = map.get("NUMBER_VALUE");
            if(rawValue.getClass().equals(BigDecimal.class))
                ch.setValue(((BigDecimal)rawValue).longValue());
            else ch.setValue(((Integer)rawValue).longValue());
        }
        else if(map.get("DATE_VALUE")!=null) {
            ch = new CharacteristicImpl<Timestamp>();
            ch.setCharType(CharType.DATE);
            ch.setValue(map.get("DATE_VALUE"));
        }
        if(ch != null) {
            ch.setName((String) map.get("ATTRIBUTE_NAME"));
            ch.setDescription((String)map.get("ATTRIBUTE_DESC"));
            Object rawValue = map.get("ATTRIBUTE_ID");
            if(rawValue.getClass().equals(BigDecimal.class))
                ch.setId(((BigDecimal)rawValue).longValue());
            else ch.setId(((Integer)rawValue).longValue());
        }
        return ch;
    }

}

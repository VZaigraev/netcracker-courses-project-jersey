package com.ncedu.ecare.security;

import org.apache.ibatis.session.SqlSession;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.salt.SaltGenerator;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public abstract class Authorization {

    public static Token authorize(String key) {
        try{
        Token token = Token.decrypt(key);
        Long timePassed = Instant.now().getEpochSecond()-token.getTime().getEpochSecond();
        if(timePassed>=token.getDuration())
            throw new ForbiddenException();
        Long exists;
        try(SqlSession session = SessionFactory.getFactory().openSession()) {
            exists = session.selectOne("checkUser",token.getLogin());
        }
        if (exists == 0L)
            throw new ForbiddenException();
        return token;
        }
        catch (Exception e) {
            return null;
        }
    }

    public static void register(String login, String password) throws IOException{
        SaltGenerator generator = new RandomSaltGenerator();
        String salt = new String(generator.generateSalt(16));
        HashMap<String,Object> map = new HashMap<>();
        map.put("LOGIN",login);
        map.put("SALT",salt);
        map.put("PASSWORD",(salt+password).hashCode());
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            session.insert("insertUser",map);
            session.commit();
        }
    }

    public static String getToken(String login, String password) throws ForbiddenException, NotFoundException {
        Map map = null;
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            map = session.selectOne("selectUser",login);
        }
        if(map==null) throw new NotFoundException();
        String salt =(String) map.get("SALT");
        BigDecimal passDec = (BigDecimal) map.get("PASSWORD");
        Integer passInt = passDec.intValue();
        if(!passInt.equals((salt+password).hashCode())) throw new ForbiddenException();
        Token token = Token.generate(login,3600L);
        return token.encrypt();
    }
}

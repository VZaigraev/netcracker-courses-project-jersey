package com.ncedu.ecare.security.jersey;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/")
public class RestApplication extends ResourceConfig {
    public RestApplication() {
        packages("com.ncedu.ecare.security.jersey.api");
        register(JacksonFeature.class);
    }


}
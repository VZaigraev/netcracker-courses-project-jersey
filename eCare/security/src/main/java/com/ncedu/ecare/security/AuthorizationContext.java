package com.ncedu.ecare.security;


import javax.ws.rs.core.SecurityContext;
import java.security.Principal;


public class AuthorizationContext implements SecurityContext {
    private String role;
    private String login;
    private boolean isSecure;

    private class User implements Principal{
        private String name;

        private User(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

    }

    public AuthorizationContext(String role, String login, boolean isSecure) {
        this.role = role;
        this.login = login;
        this.isSecure = isSecure;
    }

    @Override
    public Principal getUserPrincipal() {
        return new User(login);
    }

    @Override
    public boolean isUserInRole(String s) {
        return role.equals(s);
    }

    @Override
    public boolean isSecure() {
        return isSecure;
    }

    @Override
    public String getAuthenticationScheme() {
        return "API-KEY";
    }
}

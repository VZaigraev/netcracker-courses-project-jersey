package com.ncedu.ecare.security;

import org.apache.ibatis.session.SqlSession;
import org.jasypt.util.text.StrongTextEncryptor;

import java.time.Instant;
import java.util.Map;

public class Token {
    private static final String ADMIN = "admin";
    private Instant time;
    private Long duration;
    private String login;

    private Token() {
    }

    private static final StrongTextEncryptor encryptor = init();

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private static StrongTextEncryptor init(){
        StrongTextEncryptor encryptor = new StrongTextEncryptor();
        encryptor.setPassword("SAFESALT");
        return encryptor;
    }

    public static Token decrypt(String encrypted) throws NullPointerException{
        Token token = new Token();
        String decryptedRaw = encryptor.decrypt(encrypted);
        String[] decryptedValues = decryptedRaw.split(";");
        token.setLogin(decryptedValues[0]);
        String finalRaw = encryptor.decrypt(decryptedValues[1]);
        String[] finalValues = finalRaw.split(";");
        token.setDuration(Long.decode(finalValues[1]));
        token.setTime(Instant.ofEpochSecond(Long.decode(finalValues[2])));
        return token;
    }

    public static Token generate(String login, Long duration) {
        Token token = new Token();
        token.setTime(Instant.now());
        token.setLogin(login);
        token.setDuration(duration);
        return token;
    }

    public String encrypt() {
        String value = null;
        String salt = getSalt(login);
        value = salt+';' + duration.toString() + ';' + time.getEpochSecond();
        String encryptedFirst = encryptor.encrypt(value);
        return encryptor.encrypt(login + ';' + encryptedFirst);
    }

    private static String getSalt(String login) {
        String salt = null;
        try(SqlSession session = SessionFactory.getFactory().openSession()){
           Map map = session.selectOne("selectUser",login);
           salt =(String) map.get("SALT");
        }
        return salt;
    }

    public String getRole() {
        return login.equals(ADMIN)?ADMIN:"user";
    }

    @Override
    public String toString() {
        return "Token{" +
                "time=" + time +
                ", duration=" + duration +
                ", login='" + login + '\'' +
                '}';
    }



}

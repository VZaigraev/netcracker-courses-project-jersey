package com.ncedu.ecare.security.jersey.api;


import com.ncedu.ecare.security.Authorization;
import com.ncedu.ecare.security.SessionFactory;
import com.ncedu.ecare.security.Token;
import org.apache.ibatis.session.SqlSession;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.salt.SaltGenerator;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Path(value = "/")
public class AuthorizationAPI {

    @POST
    @Path(value = "/gettoken/")
    public Response getToken(@FormParam("login") String login,
                             @FormParam("password") String password) {
       try{
           String token = Authorization.getToken(login,password);
           return Response.ok(token).build();
       } catch (WebApplicationException e) {
           return Response.status(Response.Status.FORBIDDEN).build();
       }
    }

    @POST
    @Path(value = "/register/")
    public Response register(@FormParam("login")String login,
                             @FormParam("password") String password) {
        try{
            Authorization.register(login,password);
            return Response.status(Response.Status.CREATED).build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }
}

1. Take a look on maven project structure.
2. Implement datamodel classes.
3. Implement backend (JAVA EE):
    a) connection to DB / main operations with objects (create, update, delete)
    b) add logging with the help of ch.qos (logback). find how to configure it.
    c) implement backend RESTs (they should return JSONs):
        - get all phones / one phone by id
        - get all accessories / one accessory by id
        - get all services / one service by id
        (*) be ready for implementing filtering, paginating.
    (*) Authorization / support of 'remote' clients.

4. Implement frontend (Spring):
    a) connect to server and send requests for data
    b) simple design, functionality
    c) log in / log off customer
    d) showing catalog + special offers
    e) initiating SalesOrder

5. Add issue support on bitbucket (I will create tickets there with tasks, bugs and estimations)

Additionally (if we do tasks well):
    use Weka ML framework for building (this will be discussed later):
        - suggesting goods for particular customer via RF
        - classifying customers by their sales order (Admin Tools tab)
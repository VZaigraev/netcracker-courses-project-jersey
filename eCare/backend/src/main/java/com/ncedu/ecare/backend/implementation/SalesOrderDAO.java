package com.ncedu.ecare.backend.implementation;

import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.implementation.SalesOrderImpl;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.util.List;

import static com.ncedu.ecare.datamodel.ObjectTypeId.SALESORDER;

public class SalesOrderDAO extends DataAccessObject<SalesOrderImpl> {
    @Override
    public void insert(SalesOrderImpl value) throws IOException {
        super.insert(value, SALESORDER.getValue());
    }

    public List<SalesOrderImpl> select (String attributeName, String selectType, List<Filter> filterList, int limit, int offset) throws IOException {
        return select(SALESORDER.getValue(),attributeName, selectType,filterList, limit, offset);
    }

    @Override
    public void delete(Long id) throws IOException {
        super.delete(id, SALESORDER.getValue());
    }

    @Override
    public Long getCount() throws IOException {
        return getCount(SALESORDER.getValue());
    }

    @Override
    public void update(SalesOrderImpl value) throws IOException {
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            session.update("updateObject",value.toHashMap());
            session.commit();
        }
        CharacteristicDAO.delete(value.getId());
        CharacteristicDAO.insert(value.getId(),value.takeChars());
    }


}

package com.ncedu.ecare.backend.dao;

import com.ncedu.ecare.implementation.DataImpl;

import java.io.IOException;
import java.util.List;


public interface DataDAO<T extends DataImpl> {
    T get(Long id) throws IOException;
    void insert (T value) throws IOException;
    void update (T value) throws IOException;
    void delete (Long id) throws IOException;
    Long getCount() throws IOException;
    List<T> select(String attributeName, String selectType, List<Filter> filter, int limit, int offset) throws IOException;
}

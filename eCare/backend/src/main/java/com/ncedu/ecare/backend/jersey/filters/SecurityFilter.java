package com.ncedu.ecare.backend.jersey.filters;

import com.ncedu.ecare.security.Authorization;
import com.ncedu.ecare.security.AuthorizationContext;
import com.ncedu.ecare.security.Token;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
public class SecurityFilter implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        SecurityContext previous = containerRequestContext.getSecurityContext();
        Token token = Authorization.authorize(containerRequestContext.getHeaderString("APIKEY"));
        if(token==null) containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                .entity("User must be authorised to proceed this request").build());
        else {
            containerRequestContext.setSecurityContext(
                    new AuthorizationContext(token.getRole(),token.getLogin(),previous.isSecure()));
        }
    }
}

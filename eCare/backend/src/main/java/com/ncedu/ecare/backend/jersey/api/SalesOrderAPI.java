package com.ncedu.ecare.backend.jersey.api;


import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.backend.dao.SelectType;
import com.ncedu.ecare.backend.implementation.SalesOrderDAO;
import com.ncedu.ecare.implementation.SalesOrderImpl;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Path(value = "/salesorder")
@Api(value = "Sales Order")
public class SalesOrderAPI {
    private static final SalesOrderDAO DAO = init();
    private static SalesOrderDAO init(){
        return new SalesOrderDAO();
    }
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Find SalesOrder by ID",
            response = SalesOrderImpl.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 404, message = "SalesOrder not found") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIdOrder(@ApiParam(value = "ID of SalesOrder to be fetched", required = true) @PathParam("id") long id) {
        try{
            SalesOrderImpl val =  DAO.get(id);
            return Response.ok(val).build();
        }
        catch (java.io.IOException e) {
            return Response.status(404).build();
        }

    }

    @POST
    @Path("/select")
    @ApiOperation(value = "Select list of SalesOrders",
            response = SalesOrderImpl.class, responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response selectOrder(@ApiParam(value = "Choose column to sort the list by",allowableValues = "name, client, date, products",required = false)
                           @QueryParam("orderField") String field,
                           @ApiParam(value = "Choose sorting destination",allowableValues = "desc, asc", required = false)
                           @QueryParam("orderDest") String dest,
                           @ApiParam(value = "Limit output rows", required = true)
                           @QueryParam("limit") int limit,
                           @ApiParam(value = "Set offset", required = true)
                           @QueryParam("offset") int offset,
                           @ApiParam(value = "List of filters, applied to the query", required = false)
                                   List<Filter> filterList) {
        try {
            List<SalesOrderImpl> SalesOrders = DAO.select(field,SelectType.getValue(dest),filterList,limit,offset);
            return Response.ok(SalesOrders).build();
        }
        catch(IOException e) {
            return Response.serverError().build();
        }
    }


    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Delete SalesOrder by Id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteOrder(@ApiParam(value = "ID of SalesOrder to be deleted", required = true) @PathParam("id") long id) {
        try {
            DAO.delete(id);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/count")
    @ApiOperation(value = "Get count of SalesOrders")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrderCount() {
        try {
            Long value = DAO.getCount();
            return Response.ok(value).build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/")
    @ApiOperation(value = "Insert new SalesOrder")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful insertion"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertOrder (@ApiParam(value = "New SalesOrder object", required = true)
                                    SalesOrderImpl value) {
        try {
            DAO.insert(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/")
    @ApiOperation(value = "Update SalesOrder")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateOrder (@ApiParam(value = "Updated SalesOrder object", required = true)
                                    SalesOrderImpl value) {
        try {
            DAO.update(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }
}

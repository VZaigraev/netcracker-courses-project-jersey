package com.ncedu.ecare.backend.jersey.api;


import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.backend.dao.SelectType;
import com.ncedu.ecare.backend.implementation.ClientDAO;
import com.ncedu.ecare.implementation.ClientImpl;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Path(value="/client")
@Api(value = "Client")
public class ClientAPI {
    private static final ClientDAO DAO = init();
    private static ClientDAO init() {return new ClientDAO(); }

    @GET
    @Path("/{id}")
    @ApiOperation(value = "Find Client by ID",
            response = ClientImpl.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 404, message = "Client not found") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIdClient(@ApiParam(value = "ID of client to be fetched", required = true) @PathParam("id") long id) {
        try{
            ClientImpl val =  DAO.get(id);
            return Response.ok(val).build();
        }
        catch (java.io.IOException e) {
            return Response.status(404).build();
        }

    }

    @POST
    @Path("/select")
    @ApiOperation(value = "Select list of Clients",
            response = ClientImpl.class, responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response selectClient(@ApiParam(value = "Choose column to sort the list by",allowableValues = "name, fio, address",required = false)
                           @QueryParam("orderField") String field,
                           @ApiParam(value = "Choose sorting destination",allowableValues = "desc, asc", required = false)
                           @QueryParam("orderDest") String dest,
                           @ApiParam(value = "Limit output rows", required = true)
                           @QueryParam("limit") int limit,
                           @ApiParam(value = "Set offset", required = true)
                           @QueryParam("offset") int offset,
                           @ApiParam(value = "List of filters, applied to the query", required = false)
                                   List<Filter> filterList) {
        try {
            List<ClientImpl> clients = DAO.select(field,SelectType.getValue(dest),filterList,limit,offset);
            return Response.ok(clients).build();
        }
        catch(IOException e) {
            return Response.serverError().build();
        }
    }


    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Delete Client by Id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteClient(@ApiParam(value = "ID of Client to be deleted", required = true) @PathParam("id") long id) {
        try {
            DAO.delete(id);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/count")
    @ApiOperation(value = "Get count of Clients")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClientCount() {
        try {
            Long value = DAO.getCount();
            return Response.ok(value).build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/")
    @ApiOperation(value = "Insert new Client")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful insertion"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertClient (@ApiParam(value = "New Client object", required = true)
                                    ClientImpl value) {
        try {
            DAO.insert(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/")
    @ApiOperation(value = "Update Client")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateClient (@ApiParam(value = "Updated Client object", required = true)
                                    ClientImpl value) {
        try {
            DAO.update(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

}

package com.ncedu.ecare.backend.implementation;

import com.ncedu.ecare.backend.dao.DataDAO;
import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.datamodel.CharacteristicId;
import com.ncedu.ecare.implementation.DataImpl;
import com.ncedu.ecare.implementation.DataObjectFactory;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public abstract class DataAccessObject<T extends DataImpl> implements DataDAO<T> {
    private static final String OBJECT_ID = "OBJECT_ID";
    private static final String OBJECT_TYPE_ID = "OBJECT_TYPE_ID";

    public T get(Long id) throws IOException {
        HashMap map = null;
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            map = session.selectOne("selectObjectId",id);
        }
        return (T)DataObjectFactory.getObject(id,map,CharacteristicDAO.get(id));
    }

    protected Long getCount(Long typeId) {
        Long value = null;
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            value = session.selectOne("selectCount",typeId);
        }
        return value;
    }

    protected void insert(T value,Long typeId) throws IOException {
        Long id = null;
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            id = session.selectOne("selectNewId");
            HashMap map = value.toHashMap();
            map.put(OBJECT_ID,id);
            map.put(OBJECT_TYPE_ID,typeId);
            session.insert("insertObject",map);
            session.commit();
        }
        CharacteristicDAO.insert(id,value.takeChars());
    }

    public void update(T value) throws IOException {
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            session.update("updateObject",value.toHashMap());
            session.commit();
        }
        CharacteristicDAO.update(value.getId(),value.takeChars());
    }

    protected void delete(Long id, Long typeId) throws IOException {
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            HashMap<String,Object> params= new HashMap<String,Object>();
            params.put(OBJECT_TYPE_ID,typeId);
            params.put(OBJECT_ID,id);
            session.delete("deleteObject",params);
            session.commit();
        }
    }


    protected List<T> select(Long objectType, String attributeName , String selectType, List<Filter> filterList, int limit, int offset) throws IOException {
        List<HashMap> rawList = null;
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            HashMap<String,Object> params= new HashMap<String,Object>();
            params.put(OBJECT_TYPE_ID,objectType);
            params.put("ORDER_TYPE",selectType);
            if(filterList != null){
                List<String> stringList = new ArrayList<String>();
                for (Filter filter :filterList) {
                    stringList.add(filter.toSql());
                }
                params.put("FILTER",stringList);
            }
            Long attributeID = CharacteristicId.getValue(attributeName);
            if(attributeID!=-1) {
                params.put("ATTRIBUTE_ID", CharacteristicId.getValue(attributeName));
            } else {
                params.put("NAME_ID",CharacteristicId.getValue(attributeName));
            }
            rawList = session.selectList("selectTypeByChar", params,
                    new RowBounds(offset, limit));
        }
        List<T> objectsList = new ArrayList<T>();
        for (HashMap map: rawList) {
            BigDecimal id =(BigDecimal) map.get("OBJECT_ID");
            objectsList.add((T)DataObjectFactory.getObject(id.longValue(),
                    map,CharacteristicDAO.get(id.longValue())));
        }
        return objectsList;
    }
}

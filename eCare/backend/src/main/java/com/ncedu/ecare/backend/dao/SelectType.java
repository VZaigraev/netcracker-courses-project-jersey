package com.ncedu.ecare.backend.dao;


public enum SelectType {
    ASC("ASC"), DESC("DESC");
    private String value;

    SelectType(String value) {
        this.value = value;
    }

    public static String getValue(String name){
        try {
            return valueOf(name.toUpperCase()).value;
        }
        catch (Exception e){
            return ASC.value;
        }
    }

    public String getValue() {
        return value;
    }

}

package com.ncedu.ecare.backend.implementation;

import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.implementation.ClientImpl;

import java.io.IOException;
import java.util.List;

import static com.ncedu.ecare.datamodel.ObjectTypeId.CLIENT;

public class ClientDAO extends DataAccessObject<ClientImpl>{
    @Override
    public void insert(ClientImpl value) throws IOException {
        insert(value, CLIENT.getValue());
    }

    @Override
    public void delete(Long id) throws IOException {
        super.delete(id, CLIENT.getValue());
    }

    @Override
    public Long getCount() throws IOException {
        return getCount(CLIENT.getValue());
    }

    public List<ClientImpl> select (String attributeName, String selectType, List<Filter> filterList, int limit, int offset) throws IOException {
        return select(CLIENT.getValue(),attributeName, selectType,filterList, limit, offset);
    }
}

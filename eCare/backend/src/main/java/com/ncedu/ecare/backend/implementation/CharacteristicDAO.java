package com.ncedu.ecare.backend.implementation;


import com.ncedu.ecare.datamodel.Characteristic;
import com.ncedu.ecare.implementation.DataObjectFactory;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public abstract class CharacteristicDAO {
    private static final String TEXT_VALUE = "TEXT_VALUE";
    private static final String NUMBER_VALUE = "NUMBER_VALUE";
    private static final String OBJECT_ID = "OBJECT_ID";

    public static List<Characteristic> get(Long id) throws IOException {
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            List<HashMap> rawList = session.selectList("selectChar",id);
            List<Characteristic> list = new ArrayList<Characteristic>();
            for (HashMap map: rawList) {
                Characteristic ch = DataObjectFactory.getChar(map);
                if(ch != null) {
                    list.add(ch);
                }
            }
            return list;
        }
    }


    public static void update(Long objectId,List<Characteristic> value) throws IOException {
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            for (Characteristic ch: value) {
                HashMap<String,Object> map = ch.toHashMap();
                map.put(OBJECT_ID,objectId);
                session.update("updateChar",map);
            }
            session.commit();
        }
    }
    public static void insert(Long objectId, List<Characteristic> value) throws IOException {
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            for (Characteristic ch: value) {
                HashMap<String,Object> map = ch.toHashMap();
                map.put(OBJECT_ID,objectId);
                session.insert("insertChar",map);
            }
            session.commit();
        }
    }

    public static void delete(Long objectId) throws IOException {
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            session.delete("deleteObjectChars",objectId);
            session.commit();
        }
    }


}

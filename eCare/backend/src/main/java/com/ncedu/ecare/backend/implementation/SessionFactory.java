package com.ncedu.ecare.backend.implementation;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.Reader;

public abstract class SessionFactory {
    private static SqlSessionFactory factory;
    static {
        try{
            Reader reader = Resources.getResourceAsReader("config.xml");
            factory = new SqlSessionFactoryBuilder().build(reader);
        } catch (java.io.IOException e){
            throw new ExceptionInInitializerError(e);
        }
    }
    public static SqlSessionFactory getFactory() {
        return factory;
    }
}

package com.ncedu.ecare.backend.jersey.api;


import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.backend.dao.SelectType;
import com.ncedu.ecare.backend.implementation.AccessoryDAO;
import com.ncedu.ecare.implementation.AccessoryImpl;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Path(value="/accessory" )
@Api(value = "Accessory")
public class AccessoryAPI {

    private static final AccessoryDAO DAO = init();
    private static AccessoryDAO init() { return new AccessoryDAO(); }

    @GET
    @Path("/{id}")
    @ApiOperation(value = "Find Accessory by ID",
            response = AccessoryImpl.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 404, message = "Accessory not found") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIdAccessory(@ApiParam(value = "ID of Accessory to be fetched", required = true) @PathParam("id") long id) {
        try{
            AccessoryImpl val =  DAO.get(id);
            return Response.ok(val).build();
        }
        catch (java.io.IOException e) {
            return Response.status(404).build();
        }

    }

    @POST
    @Path("/select")
    @ApiOperation(value = "Select list of Accessories",
            response = AccessoryImpl.class, responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response selectAccessory(@ApiParam(value = "Choose column to sort the list by",allowableValues = "name, type, color",required = false)
                           @QueryParam("orderField") String field,
                           @ApiParam(value = "Choose sorting destination",allowableValues = "desc, asc", required = false)
                           @QueryParam("orderDest") String dest,
                           @ApiParam(value = "Limit output rows", required = true)
                           @QueryParam("limit") int limit,
                           @ApiParam(value = "Set offset", required = true)
                           @QueryParam("offset") int offset,
                           @ApiParam(value = "List of filters, applied to the query", required = false)
                           List<Filter> filterList) {
        try {
            List<AccessoryImpl> accessories = DAO.select(field,SelectType.getValue(dest),filterList,limit,offset);
            return Response.ok(accessories).build();
        }
        catch(IOException e) {
            return Response.serverError().build();
        }
    }


    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Delete Accessory by Id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAccessory(@ApiParam(value = "ID of Accessory to be deleted", required = true) @PathParam("id") long id) {
        try {
            DAO.delete(id);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/count")
    @ApiOperation(value = "Get count of Accessories")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccessoryCount() {
        try {
            Long value = DAO.getCount();
            return Response.ok(value).build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/")
    @ApiOperation(value = "Insert new Accessory")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful insertion"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertAccessory (@ApiParam(value = "New Accessory object", required = true)
                                        AccessoryImpl value) {
        try {
            DAO.insert(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/")
    @ApiOperation(value = "Update Accessory")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateAccessory (@ApiParam(value = "Updated Accessory object", required = true)
                                        AccessoryImpl value) {
        try {
            DAO.update(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

}

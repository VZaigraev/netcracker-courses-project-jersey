package com.ncedu.ecare.backend.jersey.api;

import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.backend.dao.SelectType;
import com.ncedu.ecare.backend.implementation.ServiceDAO;
import com.ncedu.ecare.implementation.ServiceImpl;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;




@Path(value = "/service")
@Api(value = "Service")
public class ServiceAPI {
    private static final ServiceDAO DAO = init();
    private static ServiceDAO init(){
        return new ServiceDAO();
    }
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Find Service by ID",
            response = ServiceImpl.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 404, message = "Service not found") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIdService(@ApiParam(value = "ID of Service to be fetched", required = true) @PathParam("id") long id) {
        try{
            ServiceImpl val =  DAO.get(id);
            return Response.ok(val).build();
        }
        catch (java.io.IOException e) {
            return Response.status(404).build();
        }

    }

    @POST
    @Path("/select")
    @ApiOperation(value = "Select list of Services",
            response = ServiceImpl.class, responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response selectService(@ApiParam(value = "Choose column to sort the list by",allowableValues = "name, sms, internet, calls",required = false)
                           @QueryParam("orderField") String field,
                           @ApiParam(value = "Choose sorting destination",allowableValues = "desc, asc", required = false)
                           @QueryParam("orderDest") String dest,
                           @ApiParam(value = "Limit output rows", required = true)
                           @QueryParam("limit") int limit,
                           @ApiParam(value = "Set offset", required = true)
                           @QueryParam("offset") int offset,
                           @ApiParam(value = "List of filters, applied to the query", required = false)
                                   List<Filter> filterList) {
        try {
            List<ServiceImpl> services = DAO.select(field,SelectType.getValue(dest),filterList,limit,offset);
            return Response.ok(services).build();
        }
        catch(IOException e) {
            return Response.serverError().build();
        }
    }


    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Delete Service by Id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteService(@ApiParam(value = "ID of Service to be deleted", required = true) @PathParam("id") long id) {
        try {
            DAO.delete(id);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/count")
    @ApiOperation(value = "Get count of Services")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServiceCount() {
        try {
            Long value = DAO.getCount();
            return Response.ok(value).build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/")
    @ApiOperation(value = "Insert new Service")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful insertion"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertService (@ApiParam(value = "New Service object", required = true)
                                    ServiceImpl value) {
        try {
            DAO.insert(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/")
    @ApiOperation(value = "Update Service")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateService (@ApiParam(value = "Updated Service object", required = true)
                                    ServiceImpl value) {
        try {
            DAO.update(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }
}

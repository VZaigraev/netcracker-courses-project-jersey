package com.ncedu.ecare.backend.implementation;

import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.implementation.ServiceImpl;

import java.io.IOException;
import java.util.List;

import static com.ncedu.ecare.datamodel.ObjectTypeId.SERVICE;


public class ServiceDAO extends DataAccessObject<ServiceImpl> {

    public void insert(ServiceImpl value) throws IOException {
        insert(value, SERVICE.getValue());
    }

    public List<ServiceImpl> select (String attributeName, String selectType, List<Filter> filterList, int limit, int offset) throws IOException {
        return select(SERVICE.getValue(),attributeName, selectType,filterList, limit, offset);
    }


    @Override
    public void delete(Long id) throws IOException {
        super.delete(id, SERVICE.getValue());
    }

    @Override
    public Long getCount() throws IOException {
        return getCount(SERVICE.getValue());
    }
}

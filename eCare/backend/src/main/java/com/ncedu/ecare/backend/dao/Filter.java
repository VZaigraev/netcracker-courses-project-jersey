package com.ncedu.ecare.backend.dao;

import com.ncedu.ecare.datamodel.CharacteristicId;

public class Filter {
    private String attribute;
    private String from;
    private String to;

    public Filter() {
    }

    public Filter(String attribute, String from, String to) {
        this.attribute = attribute;
        this.from = from;
        this.to = to;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String toSql(){
        String sql = "";
        String type = CharacteristicId.getType(attribute);
        Long id = CharacteristicId.getValue(attribute);
        if(id !=-1L) {
            sql = "o.OBJECT_ID IN (SELECT pp.OBJECT_ID FROM PARAMS pp WHERE pp.ATTRIBUTE_ID = " +
                    id + " AND "+
                    CharacteristicId.getType(attribute) + " BETWEEN ";
            switch (type) {
                case "TEXT_VALUE":
                    sql += '\'' + from + "\' AND \'" + to + "\' )";
                    break;
                case "DATE_VALUE":
                    sql += "CAST(TO_TIMESTAMP(\'" + from + "\',\'YYYY-MM-DD HH24:MI:SS,FF1\') AS DATE) AND" +
                            " CAST(TO_TIMESTAMP(\'" + to + "\',\'YYYY-MM-DD HH24:MI:SS,FF1\') AS DATE) )";
                    break;
                case "NUMBER_VALUE":
                    sql += from + " AND " + to + " )";
                    break;
            }
        }
        else {
            sql = "o.OBJECT_NAME BETWEEN \'" + from + "\' AND \'" + to +'\'';
        }
        return sql;
    }
}

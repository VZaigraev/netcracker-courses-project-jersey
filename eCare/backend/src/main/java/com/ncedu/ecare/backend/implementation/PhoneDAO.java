package com.ncedu.ecare.backend.implementation;

import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.implementation.PhoneImpl;

import java.io.IOException;
import java.util.List;

import static com.ncedu.ecare.datamodel.ObjectTypeId.PHONE;

public class PhoneDAO extends DataAccessObject<PhoneImpl> {
    @Override
    public void insert(PhoneImpl value) throws IOException {
        insert(value, PHONE.getValue());
    }

    @Override
    public void delete(Long id) throws IOException {
        super.delete(id, PHONE.getValue());
    }

    @Override
    public Long getCount() throws IOException {
        return getCount(PHONE.getValue());
    }

    public List<PhoneImpl> select (String attributeName, String selectType, List<Filter> filterList, int limit, int offset) throws IOException {
        return select(PHONE.getValue(),attributeName, selectType,filterList, limit, offset);
    }
}

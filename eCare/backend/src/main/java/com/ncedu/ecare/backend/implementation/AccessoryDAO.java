package com.ncedu.ecare.backend.implementation;

import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.implementation.AccessoryImpl;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.util.List;

import static com.ncedu.ecare.datamodel.ObjectTypeId.ACCESSORY;

public class AccessoryDAO extends DataAccessObject<AccessoryImpl> {
    @Override
    public void insert(AccessoryImpl value) throws IOException {
        insert(value, ACCESSORY.getValue());
    }

    @Override
    public void delete(Long id) throws IOException {
        super.delete(id, ACCESSORY.getValue());
    }

    @Override
    public Long getCount() throws IOException {
        return getCount(ACCESSORY.getValue());
    }

    public List<AccessoryImpl> select (String attributeName, String selectType, List<Filter> filterList, int limit, int offset) throws IOException {
        return select(ACCESSORY.getValue(),attributeName, selectType,filterList, limit, offset);
    }

    @Override
    public void update(AccessoryImpl value) throws IOException {
        try(SqlSession session = SessionFactory.getFactory().openSession()){
            session.update("updateObject",value.toHashMap());
            session.commit();
        }
        CharacteristicDAO.delete(value.getId());
        CharacteristicDAO.insert(value.getId(),value.takeChars());
    }
}

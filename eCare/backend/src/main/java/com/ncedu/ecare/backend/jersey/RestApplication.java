package com.ncedu.ecare.backend.jersey;


import io.swagger.jaxrs.config.BeanConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import javax.ws.rs.ApplicationPath;


@ApplicationPath("api")
public class RestApplication extends ResourceConfig {
    public RestApplication() {
        packages("com.ncedu.ecare.backend.jersey.api",
                "com.ncedu.ecare.backend.jersey.filters");
        register(JacksonFeature.class);
        register(io.swagger.jaxrs.listing.ApiListingResource.class);
        register(io.swagger.jaxrs.listing.SwaggerSerializers.class);
        register(RolesAllowedDynamicFeature.class);

        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/api");
        beanConfig.setResourcePackage("com.ncedu.ecare.backend.jersey.api");
        beanConfig.setScan(true);

    }


}

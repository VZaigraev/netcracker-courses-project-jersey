package com.ncedu.ecare.backend.jersey.api;


import com.ncedu.ecare.backend.dao.Filter;
import com.ncedu.ecare.backend.dao.SelectType;
import com.ncedu.ecare.backend.implementation.PhoneDAO;
import com.ncedu.ecare.implementation.PhoneImpl;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Path(value = "/phone")
@Api(value = "Phone")
public class PhoneAPI {
    private static final PhoneDAO DAO = init();
    private static PhoneDAO init() { return new PhoneDAO(); }

    @GET
    @Path("/{id}")
    @ApiOperation(value = "Find Phone by ID",
            response = PhoneImpl.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 404, message = "Phone not found") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIdPhone(@ApiParam(value = "ID of Phone to be fetched", required = true) @PathParam("id") long id) {
        try{
            PhoneImpl val =  DAO.get(id);
            return Response.ok(val).build();
        }
        catch (java.io.IOException e) {
            return Response.status(404).build();
        }

    }

    @POST
    @Path("/select")
    @ApiOperation(value = "Select list of Phones",
            response = PhoneImpl.class, responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response selectPhone(@ApiParam(value = "Choose column to sort the list by",allowableValues = "name, screensize, operatingsystem, color",required = false)
                           @QueryParam("orderField") String field,
                           @ApiParam(value = "Choose sorting destination",allowableValues = "desc, asc", required = false)
                           @QueryParam("orderDest") String dest,
                           @ApiParam(value = "Limit output rows", required = true)
                           @QueryParam("limit") int limit,
                           @ApiParam(value = "Set offset", required = true)
                           @QueryParam("offset") int offset,
                           @ApiParam(value = "List of filters, applied to the query", required = false)
                                   List<Filter> filterList) {
        try {
            List<PhoneImpl> phones = DAO.select(field,SelectType.getValue(dest),filterList,limit,offset);
            return Response.ok(phones).build();
        }
        catch(IOException e) {
            return Response.serverError().build();
        }
    }


    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Delete Phone by Id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePhone(@ApiParam(value = "ID of Phone to be deleted", required = true) @PathParam("id") long id) {
        try {
            DAO.delete(id);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/count")
    @ApiOperation(value = "Get count of Phones")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPhoneCount() {
        try {
            Long value = DAO.getCount();
            return Response.ok(value).build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/")
    @ApiOperation(value = "Insert new Phone")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful insertion"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertPhone (@ApiParam(value = "New Phone object", required = true)
                                    PhoneImpl value) {
        try {
            DAO.insert(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/")
    @ApiOperation(value = "Update Phone")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update"),
            @ApiResponse(code = 500, message = "Error while processing the query") })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatePhone (@ApiParam(value = "Updated Phone object", required = true)
                                    PhoneImpl value) {
        try {
            DAO.update(value);
            return Response.ok().build();
        } catch (IOException e) {
            return Response.serverError().build();
        }
    }

}

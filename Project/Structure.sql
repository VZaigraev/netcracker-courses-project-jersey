CREATE TABLE Object_Type 
(
object_type_id  Number CONSTRAINT object_type_pk PRIMARY KEY,
type_name       VARCHAR2(255) NOT NULL
);

CREATE TABLE Object_
(
object_id NUMBER CONSTRAINT object_pk PRIMARY KEY,
object_type_id NUMBER CONSTRAINT object_fk REFERENCES object_type (object_type_id) NOT NULL,
object_name VARCHAR(255) NOT NULL
);


CREATE TABLE Attribute_
(
attribute_id number CONSTRAINT attribute_pk PRIMARY KEY,
attribute_name VARCHAR(255) NOT NULL
);

CREATE TABLE attribute_object
(
object_type_id number constraint attribute_obj_fk_obj
REFERENCES object_type (object_type_id) NOT NULL,
attribute_id number constraint attribute_obj_fk_att
REFERENCES attribute_ (attribute_id) NOT NULL
);


CREATE TABLE Params
(
object_id number CONSTRAINT params_pk_obj_id REFERENCES Object_ (object_id) NOT NULL,
attrubute_id number CONSTRAINT params_pk_att_id REFERENCES Attribute_ (attribute_id) NOT NULL,
text_value VARCHAR(255),
data_value BLOB,
number_value NUMBER
);
CREATE OR REPLACE PROCEDURE deleteObject
(id_obj IN NUMBER)
AS
BEGIN
DELETE FROM params WHERE object_id = id_obj;
DELETE FROM object_ WHERE object_id = id_obj;
END deleteObject;
